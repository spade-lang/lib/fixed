#top = lib::add_th

from spade import SpadeExt
from cocotb import cocotb
from cocotb.triggers import Timer

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut)
    s.i.a_raw = "256"
    s.i.b_raw = "512"
    await Timer(1, units='ns')
    s.o.assert_eq("3")

