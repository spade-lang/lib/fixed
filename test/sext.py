#top = lib::sext_th

from spade import SpadeExt
from cocotb import cocotb
from cocotb.triggers import Timer

@cocotb.test()
async def test(dut):
    s = SpadeExt(dut)
    s.i.a_raw = 0b0101_1111
    await Timer(1, units='ns')
    s.o.assert_eq(0b0000_0101_1111_0000)

